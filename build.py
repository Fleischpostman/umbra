import os
import sys
import platform
from subprocess import run
import argparse

parser = argparse.ArgumentParser(prog="umbra build tool",
                                 description="Build utility to run the builds for umbra")

parser.add_argument("--target", default="undefined")
args = parser.parse_args()

compiler = "clang"
if args.target == "windows-mingw-w64":
    compiler = "x86_64-w64-mingw32-gcc"
    system = "Windows-Cross-Compilation"
else:
    system = platform.system()
    
print(f"detected platform: {system}")
build_path = "build"
exe_name = "umbra"
libs = ""

if system == "Windows":
    build_path = build_path + "/windows"
    exe_name = exe_name + ".exe"
    platform_define = "-DWIN32"
elif system == "Linux":
    build_path = build_path + "/linux"
    platform_define = "-DLINUX"
    libs = "-lstdc++"
elif system == "Windows-Cross-Compilation":
    libs = "-lstdc++"
    build_path = build_path + "/windows"
    platform_define = "-DWIN32 -static"

os.makedirs(build_path, exist_ok=True)

completed_command = run(f"{compiler} src/main.cpp {platform_define} -Wall -Werror -std=c++20 -o {build_path}/{exe_name} {libs}",
                        shell=True,
                        capture_output=True,
                        text=True)

print(completed_command.stdout)
print(completed_command.stderr)
print(f"build returned: {completed_command.returncode}")

sys.exit(completed_command.returncode)
