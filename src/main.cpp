// NOTE(Fabi): We need to define this, otherwise the build will fail on windows.
//             The safe version howerver can't be used, because on linux only the
//             standard functions exist.
#define _CRT_SECURE_NO_WARNINGS

#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <filesystem>

constexpr const char* UmbraVersion = "0.1.1";
constexpr const char* SourcesFlag = "--sources";

const char** Sources = nullptr;

// Only used for debugging purposes
void PrintAllArguments(int argCount, char** args);

int main(int argCount, char** args)
{   
    if(argCount == 1)
    {
        printf("umbra - version %s\n\n", UmbraVersion);
        printf("usage: umbra --sources <sources>\n\n");
        printf("\t sources: list of directories that contain the source files that will be bundled up\n");
        printf("\n\ncommand options:\n\n");
        printf("\t--sources: all paths to files that will be included in the bundling process. An argument must be a directory\n");
    }

    for(int argIndex = 1; argIndex < argCount; argIndex++)
    {
        char* arg = args[argIndex];
        if(strcmp(arg, SourcesFlag) == 0)
        {
            argIndex++;
            int startIndex = argIndex; 
            while(argIndex < argCount)
            {
                const char* parameter = args[argIndex];
                if(parameter[0] == '-' && parameter[1] == '-')
                {
                    break;
                }

                argIndex++;
            }

            int count = argIndex - startIndex;
            for(int sourceIndex = 0; sourceIndex < count; sourceIndex++)
            {
                arrput(Sources, args[startIndex + sourceIndex]);
            }
        }
    }

    FILE* outputFile = fopen("build.cpp", "w");
    if(outputFile == nullptr)
    {
        perror("Error opening file");
        return 1;
    }

    for(int sourceIndex = 0; sourceIndex < arrlen(Sources); sourceIndex++)
    {
        const char* source = Sources[sourceIndex];
        if(std::filesystem::is_directory(source))
        {
            for(const auto& file : std::filesystem::directory_iterator(source))
            {
                if(file.path().extension() == ".cpp")
                {
                    // NOTE(Fabi): This is hilarious C++ shit. On windows the filesystem
                    //             implementation uses wchar_t instead of char. I thougt
                    //             I could fix this by issuing the build with -DUNICODE
                    //             instead of using this define, but my tests did not work
                    //             so I guess this fix will be the way to go in for the future.
                    //             From time to time it may be interesting to check if this
                    //             problem resolves itself.
                    #if WIN32
                    fprintf(outputFile, "#include \"%ls\"\n", file.path().c_str());
                    #elif LINUX
                    fprintf(outputFile, "#include \"%s\"\n", file.path().c_str());
                    #endif
                }
            }
        }
        else
        {
            printf("Warning: source '%s' is not a directory. Currently umbra only supports directories as sources. Source entry will be skipped.\n", source);
        }         
    }

    fclose(outputFile);
    return 0;
}

void PrintAllArguments(int argCount, char** args)
{
    for(int argIndex = 0; argIndex < argCount; argIndex++)
    {
        printf("[%d]: %s\n", argIndex, args[argIndex]);
    }
}