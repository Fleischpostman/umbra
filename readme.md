# Building from source

## Requirements

In order to build the project you must have installed following requirements on your machine:

- Python 3 

### Linux:

`sudo apt install libstdc++-12-dev`

## Build instructions

### Windows:

`python build.py`

### Linux:

`python3 build.py`

This will be an example usage:
umbra --sources examples\math