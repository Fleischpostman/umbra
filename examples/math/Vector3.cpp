#include "Vector3.hpp"

Vector3 Add(Vector3 a, Vector3 b)
{
    Vector3 result = { a.X + b.X, a.Y + b.Y, a.Z + b.Z };
    return result;
}