#include <stdio.h>
#include "Vector3.hpp"

int main()
{
    Vector3 a = { 1, 2, 3 };
    Vector3 b = { 3, 4, 5 };
    Vector3 c = Add(a, b);

    printf("A: {%f, %f, %f}\n", a.X, a.Y, a.Z);
    printf("B: {%f, %f, %f}\n", b.X, b.Y, b.Z);
    printf("C: {%f, %f, %f}\n", c.X, c.Y, c.Z);

    return 0;
}