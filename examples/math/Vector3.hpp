#pragma once

struct Vector3
{
    float X;
    float Y;
    float Z;
};

Vector3 Add(Vector3 a, Vector3 b);